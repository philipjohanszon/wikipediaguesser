module wikiGuesser

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/joho/godotenv v1.3.0
	github.com/kataras/go-sessions/v3 v3.3.0
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
)
