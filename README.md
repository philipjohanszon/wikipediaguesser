# wikipediaGuesser

A game website simular to geoguessr that will show you images from wikipedia and make you pick what title
is the correct title for the article that the image appeared in. To score a highscore you must get as many
correct answers in a row while racing against time. If you fail a question you will have to try again.

# About

It is primarily written in Golang. It uses html/templates for the templating and uses python for handling Wikipedias API.

