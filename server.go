package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"

	"wikiGuesser/router"
	"github.com/joho/godotenv"
)

//https://en.wikipedia.org/w/api.php?action=query&generator=random&prop=pageimages&format=json&pithumbsize=100
//https://en.wikipedia.org/w/api.php?format=json&action=query&generator=random&grnnamespace=0&prop=pageimages&format=json&pithumbsize=400 <- thumbnail height
//https://www.quora.com/Does-Wikipedia-apply-any-limits-or-restrictions-on-access-via-the-Wikimedia-API
//Go for as many as u can and get score from streak then post on leaderboard
//https://colorhunt.co/palette/226670

var wg = sync.WaitGroup{}

func main() {

	err := godotenv.Load(".env")

	if err != nil {
		fmt.Println(err)
		return 
	}

	fmt.Println("Starting server at localhost:4997")
	app := &router.App{}
	fileServer := http.FileServer(http.Dir("./public"))

	http.Handle("/public/", http.StripPrefix("/public/", fileServer))
	http.HandleFunc("/", app.Serve)

	log.Fatal(http.ListenAndServe(":4997", nil))

}
