

select `gameswithusers`.`id` AS `id`,
max(`gameswithusers`.`score`) AS `MAX(score)` 
from `wikipediaguesser`.`gameswithusers` 
INNER JOIN (
select `wikipediaguesser`.`users`.`id` AS `id`,
`wikipediaguesser`.`users`.`username` AS `username`,
`wikipediaguesser`.`gamesplayed`.`score` AS `score`,
`wikipediaguesser`.`gamesplayed`.`gamemodeId` AS `gamemodeId` 
FROM (`wikipediaguesser`.`users` join `wikipediaguesser`.`gamesplayed` on(`wikipediaguesser`.`gamesplayed`.`userId` = `wikipediaguesser`.`users`.`id`))
)
group by `gameswithusers`.`id` 
order by max(`gameswithusers`.`score`) desc