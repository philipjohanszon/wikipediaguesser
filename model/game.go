package model

import (
	"database/sql"
	"fmt"
	"math"
	"math/rand"
	"net/http"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/kataras/go-sessions/v3"
)

type GameData struct {
	ImgSrc       string
	Titles       [5]string
	CorrectTitle string
	QuestionId   int
	ScoreCount   int
}

/*
	session.Set("gameTimeLeft", "")
	session.Set("gameId", "")
	session.Set("questionId", "")
	session.Set("gameDisplayedTime", "")
	session.Set("CorrectTitle", "")
	session.Set("gameIncrement", "")
	session.Set("refreshLock", "")
*/

var _gameScoreTimeMultiplier int = 500
var _minimumScoreIncrease int = 250

//For score 0
var _lostText0 []string = []string{
	"You Are Literally Dog Water, Boxed Like A Fish",
	"WikiGuesser Has Been Hit By, Struck By A Smooth Brain Criminal 🎶🎶🎶",
	"I Swear Kanye East Can Find West Better Than You Can Find The Correct Title",
	"404 Brain Not Found",
}

//For score below average
var _lostText1 []string = []string{
	"You Have No Future, Just Be Better",
	"Just Don't Be Bad lol",
	"Your Parents Are Dissapointed",
	"You Are Just Mad Because You Are Angry",
	"THERE IS NO PASSION, THERE IS NO VISION",
}

//For score slightly above average
var _lostText2 []string = []string{
	"You Got Above Average But You Are Still Useless",
	"Your Facebook Mom Is Also Slightly Above Average",
	"I Swear You Are A Redditor",
	"I Smell The Parental Dissapointment",
}

//For score a lot above average
var _lostText3 []string = []string{
	"I Can Smell The Sweat, Take A Shower",
	"Go Do Something With Your Life",
	"You Need To Have Sex, Got Damn",
	"Clean Your Room",
}

/*

TODO CACHE IMAGES WITH LINK PREFETCH IN GAME.HTML

SELECT u.* FROM users u INNER JOIN gamesplayed ON u.id = gamesplayed.userId INNER JOIN (SELECT users.id, MAX(gamesplayed.score) FROM users INNER JOIN gamesplayed ON gamesplayed.userId = users.id GROUP BY users.id) groupedUsers ON u.id = groupedUsers.id AND gamesplayed.score = groupedUsers.score ORDER BY gamesplayed.score DESC;

*/

func GetGameData(gameId interface{}) *GameData {
	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return nil
	}
	defer db.Close()

	var imageSrc string
	var questionId int
	var correctTitle string

	queryError := db.QueryRow("SELECT questions.img, questions.id, titles.name FROM questions INNER JOIN titles ON questions.titleId = titles.id ORDER BY RAND() LIMIT 1").Scan(&imageSrc, &questionId, &correctTitle)

	fmt.Println(imageSrc)

	if queryError != nil {
		fmt.Println("Couldnt query gameData")
		return nil
	}

	fmt.Println(correctTitle)

	titles, gameTitleError := getGameTitles(correctTitle)

	if gameTitleError != nil {
		fmt.Println("gameTitleError")
		return nil
	}

	var currentScore int

	scoreQueryError := db.QueryRow("SELECT score FROM gamesplayed WHERE id = ?", gameId).Scan(&currentScore)

	if scoreQueryError != nil {
		fmt.Println("Couldnt query score in gamedata")
		return nil
	}

	return &GameData{
		ImgSrc:       imageSrc,
		Titles:       titles,
		CorrectTitle: correctTitle,
		QuestionId:   questionId,
		ScoreCount:   currentScore,
	}
}

//gets 4 game titles, the first one is the correct title
func getGameTitles(correctTitle string) ([5]string, error) {
	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return [5]string{}, fmt.Errorf("coudnt get strings")
	}
	defer db.Close()

	rows, queryError := db.Query("SELECT name FROM titles ORDER BY RAND() LIMIT 4")

	if queryError != nil {
		fmt.Println("Couldnt query gameData")
		return [5]string{}, fmt.Errorf("coudnt get strings")
	}

	var titles [5]string
	counter := 0

	for rows.Next() {
		var title string

		scanError := rows.Scan(&title)

		if scanError != nil {
			fmt.Println("Couldnt query gameData")
			return [5]string{}, fmt.Errorf("coudnt get strings")
		}

		titles[counter] = title

		counter++
	}

	//puts the correct title in last
	titles[4] = correctTitle

	titles = shuffleTitles(titles)

	return titles, nil
}

func shuffleTitles(titles [5]string) [5]string {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	for n := len(titles); n > 0; n-- {
		randIndex := r.Intn(n)
		titles[n-1], titles[randIndex] = titles[randIndex], titles[n-1]
	}

	return titles
}

func StartGame(gamemodeId int, res http.ResponseWriter, req *http.Request) bool {

	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return false
	}

	defer db.Close()

	stmt, queryError := db.Prepare("INSERT INTO gamesplayed (userId, gamemodeId) VALUES (?, ?)")

	if queryError != nil {
		fmt.Println("Couldnt query gameData")
		return false
	}

	session := sessions.Start(res, req)

	userId := session.Get("userId")

	if userId == nil {
		return false
	}

	fmt.Println(gamemodeId)

	sqlResult, execError := stmt.Exec(userId, gamemodeId)

	if execError != nil {
		fmt.Println(execError.Error())
		fmt.Println("couldnt create a games played")
		return false
	}

	gameId, lastIdError := sqlResult.LastInsertId()

	if lastIdError != nil {
		fmt.Println("failed to get Lastinsertid of game")
		return false
	}

	fmt.Println("gameid")
	fmt.Println(gameId)

	session.Set("gameId", gameId)

	//now get time and increment of gamemode

	var time int
	var increment int

	timeQueryError := db.QueryRow("SELECT time, increment FROM gamemodes WHERE id = ?", gamemodeId).Scan(&time, &increment)

	if timeQueryError != nil {
		fmt.Println("couldnt get time and increment of game")
		session.Set("gameId", nil)
		return false
	}

	session.Set("gameTimeLeft", time)
	session.Set("gameIncrement", increment)

	return true
}

func HandleGameAnswer(res http.ResponseWriter, req *http.Request) bool {
	req.ParseForm()
	session := sessions.Start(res, req)

	session.Set("refreshLock", "off")

	answer := req.Form.Get("titleChoice")

	if answer != "" {

		timeTaken := HandleTime(res, req)

		if timeTaken != 0 {

			correctTitle := session.Get("CorrectTitle")

			if fmt.Sprintf("%v", correctTitle) == answer {

				updateScore(session.Get("gameId"), timeTaken)
				insertQuestionPlayed(session.Get("questionId"), session.Get("gameId"), true)
				return true

			}

			insertQuestionPlayed(session.Get("questionId"), session.Get("gameId"), false)
		} else {
			return false
		}
	}

	EndCurrentGame(res, req)

	return false
}

func HandleTime(res http.ResponseWriter, req *http.Request) int {
	session := sessions.Start(res, req)
	//idk why but this is the default time format so i just took it from the console

	if session.GetString("gameDisplayedTime") == "" {
		return 0
	}

	sendOffTime, err := time.Parse(time.UnixDate, session.GetString("gameDisplayedTime"))

	if err != nil {
		fmt.Println(err.Error())
		fmt.Println("problem parsing time")
		return 0
	}

	timeNow := time.Now()

	timeDifference64 := timeNow.Sub(sendOffTime).Milliseconds()

	//makes milliseconds to seconds
	timeDifference64 = timeDifference64 / 1000

	timeLeft64, err := session.GetInt("gameTimeLeft")

	if err != nil {
		fmt.Println("time left error in game model checktime")
		return 0
	}

	timeDifference := int(timeDifference64)
	timeLeft := int(timeLeft64)

	if timeLeft-timeDifference > 0 {
		increment, _ := session.GetInt("gameIncrement")
		setNewTimeLeft(timeLeft-timeDifference, increment, res, req)

		return timeDifference

	} else {

		insertQuestionPlayed(session.Get("questionId"), session.Get("gameId"), false)
		EndCurrentGame(res, req)

		return 0
	}
}

func setNewTimeLeft(timeLeft int, increment int, res http.ResponseWriter, req *http.Request) {
	session := sessions.Start(res, req)

	session.Set("gameTimeLeft", timeLeft+increment)
}

func EndCurrentGame(res http.ResponseWriter, req *http.Request) {
	session := sessions.Start(res, req)

	endGameInDb(session.Get("gameId"))

	session.Set("gameTimeLeft", "")
	//gameid is reset in the controller so u can get the correct redirect
	//session.Set("gameId", "")
	session.Set("questionId", "")
	session.Set("gameDisplayedTime", "")
	session.Set("CorrectTitle", "")
	session.Set("gameIncrement", "")
	session.Set("refreshLock", "off")
}

func endGameInDb(gameId interface{}) {
	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return
	}
	defer db.Close()

	stmt, queryError := db.Prepare("UPDATE gamesplayed SET finished = true WHERE id = ?")

	if queryError != nil {
		fmt.Println("Couldnt query questionsplayed insertion")
		return
	}

	_, execError := stmt.Exec(gameId)

	if execError != nil {
		fmt.Println(execError.Error())
		fmt.Println("couldnt insert questionsplayed into db")
	}
}

func updateScore(gameId interface{}, timeTakenToAnswer int) {

	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return
	}
	defer db.Close()

	var score int

	queryError := db.QueryRow("SELECT score FROM gamesplayed WHERE id = ?", gameId).Scan(&score)

	if queryError != nil {
		fmt.Println("Couldnt get score from gamesplayed")
		return
	}

	score = score + _minimumScoreIncrease + _gameScoreTimeMultiplier/timeTakenToAnswer

	stmt, queryError := db.Prepare("UPDATE gamesplayed SET score = ? WHERE id = ?")

	if queryError != nil {
		fmt.Println("Couldnt query gamesplayed update score")
		return
	}

	_, execError := stmt.Exec(score, gameId)

	if execError != nil {
		fmt.Println(queryError.Error())
		fmt.Println("couldnt update gamesplayed score")
	}

}

func insertQuestionPlayed(questionId interface{}, gameId interface{}, wasCorrect bool) {

	if questionId != "" && gameId != "" && questionId != nil && gameId != nil {

		db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

		if err != nil {
			fmt.Println("Couldnt open DB, fix")
			return
		}
		defer db.Close()

		stmt, queryError := db.Prepare("INSERT INTO questionsplayed (gameId, questionId, wasCorrect) VALUES (?, ?, ?)")

		if queryError != nil {
			fmt.Println("Couldnt query questionsplayed insertion")
			return
		}

		var wasCorrectInt int
		if wasCorrect {
			wasCorrectInt = 1
		} else {
			wasCorrectInt = 0
		}

		_, execError := stmt.Exec(gameId, questionId, wasCorrectInt)

		if execError != nil {
			fmt.Println(queryError.Error())
			fmt.Println("couldnt insert questionsplayed into db")
		}
	}
}

func GetGameResults(gameId int) interface{} {

	correctAnswer := getCorrectAnswer(gameId)
	score := GetScore(gameId)
	averageScore := GetAverageScore(gameId)
	leaderboardPosition := GetGameLeaderboardPosition(gameId)

	if correctAnswer != "" && score != -1 && averageScore != -1 && leaderboardPosition != -1 {

		lostText := getLostText(float64(score), averageScore)

		templateParams := struct {
			LostText            string
			CorrectAnswer       string
			Score               int
			AverageScore        float64
			LeaderboardPosition int
		}{
			LostText:            lostText,
			CorrectAnswer:       correctAnswer,
			Score:               score,
			AverageScore:        math.Round(averageScore),
			LeaderboardPosition: leaderboardPosition,
		}

		fmt.Println(templateParams)

		return templateParams
	}

	return nil
}

func getCorrectAnswer(gameId int) string {
	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return ""
	}
	defer db.Close()

	var correctAnswer string

	//Way to long sql, selects the correctAnswer from the game that made you fail (the one that was answeredincorrectly) and gets the title that was correct from that question
	queryError := db.QueryRow("SELECT titles.name FROM questionsplayed INNER JOIN questions ON questionsplayed.questionId = questions.id INNER JOIN titles ON questions.titleId = titles.id WHERE questionsplayed.wasCorrect = 0 AND questionsplayed.gameId = ?", gameId).Scan(&correctAnswer)

	if queryError != nil {
		fmt.Println("Couldnt query getCorrectAnswer")
		fmt.Println(queryError.Error())
		return "Correct Answer Wasn't Able To Be Found :("
	}

	return correctAnswer
}

func GetScore(gameId int) int {

	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return 0
	}
	defer db.Close()

	var score int

	queryError := db.QueryRow("SELECT score FROM gamesplayed WHERE id = ?", gameId).Scan(&score)

	if queryError != nil {
		fmt.Println("Couldnt query getScore")
		fmt.Println(queryError.Error())
		return 0
	}

	return score
}

func GetAverageScore(gameId int) float64 {

	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return 0
	}
	defer db.Close()

	var averageScore float64

	queryError := db.QueryRow("SELECT AVG(score) FROM gamesplayed WHERE finished = 1 AND gamemodeId = ?", GetGamemode(gameId)).Scan(&averageScore)

	if queryError != nil {
		fmt.Println("Couldnt query getaverageScore")
		fmt.Println(queryError.Error())
		return 0
	}

	return averageScore
}

func GetGamemode(gameId int) int {

	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return 0
	}
	defer db.Close()

	var gamemodeId int

	queryError := db.QueryRow("SELECT gamemodeId FROM gamesplayed where id = ?", gameId).Scan(&gamemodeId)

	if queryError != nil {
		fmt.Println("Couldnt query GetGamemode")
		fmt.Println(queryError.Error())
		return 0
	}

	return gamemodeId

}

func GetGameLeaderboardPosition(gameId int) int {

	//TODO

	/*db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return 0
	}
	defer db.Close()

	var leaderboardPosition int

	//Way to long sql, selects the correctAnswer from the game that made you fail (the one that was answeredincorrectly) and gets the title that was correct from that question
	queryError := db.QueryRow("SELECT id, RowNumber FROM (SELECT id, ROW_NUMBER() OVER (ORDER BY SCORE DESC) AS RowNumber FROM gamesplayed WHERE gamemodeId = ?) WHERE id = ?", GetGamemode(gameId), gameId).Scan(&leaderboardPosition)

	if queryError != nil {
		fmt.Println("Couldnt query leaderboardposition")
		fmt.Println(queryError.Error())
		return 0
	}
	*/
	return 2

}

func getLostText(score float64, averageScore float64) string {
	var lostText string = ""

	rand.Seed(time.Now().Unix())

	if float64(score) == 0 {
		lostText = _lostText0[rand.Int()%len(_lostText0)]
	} else if float64(score) <= averageScore {
		lostText = _lostText1[rand.Int()%len(_lostText1)]
	} else if float64(score) > averageScore+float64(_minimumScoreIncrease*3) {
		lostText = _lostText2[rand.Int()%len(_lostText2)]
	} else if float64(score) > averageScore+float64(_minimumScoreIncrease) {
		lostText = _lostText3[rand.Int()%len(_lostText3)]
	}

	return lostText
}

func HandleMidGameRefresh(res http.ResponseWriter, req *http.Request) interface{} {
	session := sessions.Start(res, req)

	insertQuestionPlayed(session.Get("questionId"), session.Get("gameId"), false)
	gameId := session.Get("gameId")
	session.Set("gameId", "")
	session.Set("refreshLock", "")

	return gameId
}
