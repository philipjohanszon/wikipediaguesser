package model 

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

func GetUserCount() int {
	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return 0
	}
	defer db.Close()

	var userCount int

	queryError := db.QueryRow("SELECT count(*) FROM users").Scan(&userCount)

	if queryError != nil {
		fmt.Println("Couldnt query user count")
		
		return 0
	}

	return userCount
}

func GetQuestionCount() int {
	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return 0
	}
	defer db.Close()

	var questionCount int

	queryError := db.QueryRow("SELECT count(*) FROM questions").Scan(&questionCount)

	if queryError != nil {
		fmt.Println("Couldnt query quesition count")
		
		return 0
	}

	return questionCount
}

func GetHighScore() int {
	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return 0
	}
	defer db.Close()

	var highScore int

	queryError := db.QueryRow("SELECT score FROM gamesplayed ORDER BY score DESC LIMIT 1").Scan(&highScore)
	
	if queryError != nil {
		fmt.Println(queryError.Error())
		fmt.Println("Couldnt query score")
		
		return 0
	}

	return highScore
}

func GetGamesPlayed() int {
	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return 0
	}
	defer db.Close()

	var gamesPlayed int

	queryError := db.QueryRow("SELECT count(x) FROM gamesplayed")

	if  queryError != nil {
		fmt.Println("Couldnt query")
		return 0
	}

	return gamesPlayed
}