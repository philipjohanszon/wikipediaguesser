package model

import (
	"database/sql"
	"fmt"
	"math"
	"net/http"
	"os"
	"unicode/utf8"

	_ "github.com/go-sql-driver/mysql"
	"github.com/kataras/go-sessions/v3"
	"golang.org/x/crypto/bcrypt"
)

func LoginUser(res http.ResponseWriter, req *http.Request) bool {

	req.ParseForm()

	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return false
	}
	defer db.Close()

	var password string
	var userId int

	queryError := db.QueryRow("SELECT password, id FROM users WHERE username = ?", req.Form.Get("username")).Scan(&password, &userId)

	if queryError != nil {
		fmt.Println("user not found")
		return false
	}

	if comparePassword(password, req.Form.Get("password")) {
		fmt.Println("the password matches")

		session := sessions.Start(res, req)
		session.Set("userId", userId)

		fmt.Println("session userid")
		fmt.Println(session.Get("userId"))

		return true

	} else {
		return false
	}
}

func RegisterUser(res http.ResponseWriter, req *http.Request) string {
	req.ParseForm()

	//fmt.Println(req.Form.Get("username"))
	//fmt.Println(req.Form.Get("email"))
	//fmt.Println(req.Form.Get("password"))

	if allInputsIncluded(req) {
		if passwordStrongEnough(req) {
			if req.Form.Get("email") == req.Form.Get("emailConfirm") {
				db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

				if err != nil {
					fmt.Println("Couldnt open DB, fix")
					return "dbError"
				}
				defer db.Close()

				registerCheckStmt, queryErrorWhenCheck := db.Prepare("SELECT username, email FROM users WHERE username = ? OR email = ?")

				if queryErrorWhenCheck != nil {
					fmt.Println("username and email check statement failed")
					return "queryError"
				}

				userTaken := usernameAndEmailNotTaken(registerCheckStmt, req)

				if userTaken == "notTaken" {

					stmt, queryError := db.Prepare("INSERT INTO users (username, email, password) values(?, ?, ?)")

					if queryError != nil {
						fmt.Println("insert statement failed")
						return "queryError"
					}

					newUserId := createUser(stmt, req)

					// -> 0 is a error
					if newUserId != 0 {
						//sets session (logs in)
						session := sessions.Start(res, req)
						session.Set("userId", newUserId)

						return "registered"
					}
				} else {
					//returns the return value of usernameandemailtaken
					return userTaken
				}
			} else {
				return "emailsDontMatch"
			}
		} else {
			return "passwordToWeak"
		}
	}
	return "bad request"
}

func allInputsIncluded(req *http.Request) bool {
	if req.Form.Get("username") != "" && req.Form.Get("email") != "" && req.Form.Get("password") != "" {
		return true
	} else {
		return false
	}
}

func passwordStrongEnough(req *http.Request) bool {
	if utf8.RuneCountInString(req.Form.Get("password")) > 6 {
		return true
	}
	return false
}

func usernameAndEmailNotTaken(stmt *sql.Stmt, req *http.Request) string {
	var email string
	var username string

	queryStatus := stmt.QueryRow(req.Form.Get("username"), req.Form.Get("email")).Scan(&username, &email)

	if queryStatus != nil {
		fmt.Println(queryStatus.Error())
	}

	if email != "" && email == req.Form.Get("email") {
		if username != "" && username == req.Form.Get("username") {
			return "usernameAndEmailTaken"
		} else {
			return "emailTaken"
		}
	} else if username != "" && username == req.Form.Get("username") {
		return "usernameTaken"
	}

	return "notTaken"
}

func createUser(stmt *sql.Stmt, req *http.Request) int {

	password := makePassword(req)

	queryResult, queryError := stmt.Exec(req.Form.Get("username"), req.Form.Get("email"), password)

	if queryError != nil {
		fmt.Println(queryError.Error())
		fmt.Println("user couldnt be created")
		return 0
	}

	userId, err := queryResult.LastInsertId()

	if err != nil {
		fmt.Println("failed to get Lastinsertid")
		return 0
	}

	return int(userId)
}

func makePassword(req *http.Request) string {
	password := req.Form.Get("password")
	passwordBytes := []byte(password)

	hashedPass, err := bcrypt.GenerateFromPassword(passwordBytes, 12)

	if err != nil {
		fmt.Println("FIX NOW BCRYPT COULDNT HASH PASSWORD")
	}

	return string(hashedPass)
}

func comparePassword(password string, comparer string) bool {

	hashedPass := []byte(password)
	comparePassword := []byte(comparer)

	err := bcrypt.CompareHashAndPassword(hashedPass, comparePassword)

	//if the error is nil then its a match
	if err != nil {
		return false
	} else {
		return true
	}
}

func GetProfileData(userId int) interface{} {

	type gamemodeData struct {
		Name                string
		HighScore           int
		AverageScore        float64
		GamesPlayed         int
		LeaderboardPosition int
	}

	templateParams := struct {
		Username  string
		Gamemode1 gamemodeData
		Gamemode2 gamemodeData
	}{}

	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return nil
	}
	defer db.Close()

	rows, queryError := db.Query("SELECT name, id FROM gamemodes")

	if queryError != nil {
		fmt.Println("couldnt query gamemodes")
		return nil
	}

	for rows.Next() {
		var gamemodeName string
		var gamemodeId int

		nameError := rows.Scan(&gamemodeName, &gamemodeId)

		if nameError != nil {
			fmt.Println(nameError.Error()) // proper error handling instead of panic in your app
		}

		highScore := getGamemodeHighScore(gamemodeId, userId)
		averageScore := getGamemodeAverageScore(gamemodeId, userId)
		gamesPlayed := getGamemodeGamesPlayed(gamemodeId, userId)
		leaderboardPosition := getGamemodeLeaderboardPosition(gamemodeId, userId)

		gamemodevalues := gamemodeData{
			Name:                gamemodeName,
			HighScore:           highScore,
			AverageScore:        math.Round(averageScore),
			GamesPlayed:         gamesPlayed,
			LeaderboardPosition: leaderboardPosition,
		}

		fmt.Println(gamemodevalues)

		if gamemodeId == 1 {
			templateParams.Gamemode1 = gamemodevalues
		} else if gamemodeId == 2 {
			templateParams.Gamemode2 = gamemodevalues
		}
	}

	var username string

	usernameQueryError := db.QueryRow("SELECT username FROM users WHERE id = ?", userId).Scan(&username)

	if usernameQueryError != nil {
		fmt.Println("couldnt query username")
		return nil
	}

	templateParams.Username = username

	return templateParams
}

func getGamemodeHighScore(gamemodeId int, userId int) int {
	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return 0
	}
	defer db.Close()

	var highScore int

	queryError := db.QueryRow("SELECT score FROM gamesplayed WHERE gamemodeId = ? AND userId = ? ORDER BY score DESC LIMIT 1", gamemodeId, userId).Scan(&highScore)

	if queryError != nil {
		fmt.Println(queryError.Error())
		fmt.Println("couldnt query highscore")
		return 0
	}

	return highScore
}

func getGamemodeAverageScore(gamemodeId int, userId int) float64 {
	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return 0
	}
	defer db.Close()

	var averageScore float64

	queryError := db.QueryRow("SELECT AVG(score) FROM gamesplayed WHERE gamemodeId = ? AND userId = ?", gamemodeId, userId).Scan(&averageScore)

	if queryError != nil {
		fmt.Println(queryError.Error())
		fmt.Println("couldnt query average score")
		return 0
	}

	return averageScore
}

func getGamemodeGamesPlayed(gamemodeId int, userId int) int {
	db, err := sql.Open("mysql", os.Getenv("DATABASE_CONNECTION"))

	if err != nil {
		fmt.Println("Couldnt open DB, fix")
		return 0
	}
	defer db.Close()

	var gamesPlayed int

	queryError := db.QueryRow("SELECT count(*) FROM gamesplayed WHERE gamemodeId = ? AND userId = ?", gamemodeId, userId).Scan(&gamesPlayed)

	if queryError != nil {
		fmt.Println(queryError.Error())
		fmt.Println("couldnt query gamesplayed")
		return 0
	}

	return gamesPlayed
}

func getGamemodeLeaderboardPosition(gamemodeId int, userId int) int {
	//TODO
	return 2
}
