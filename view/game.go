package view

import (
	"fmt"
	"html/template"
	"net/http"
)

type GameView struct {
}


func (h *GameView) Serve(view *View, templateParams interface{}) {
	
	if view.Path == "game" {
		parsedTemplate, _ := template.ParseFiles("html/game.html")
		err := parsedTemplate.Execute(view.Res, templateParams)

		if err != nil {
			fmt.Println("error rendering game page")
			return
		}
	} else if view.Path == "games" {
		http.ServeFile(view.Res, view.Req, "html/games.html")
	} else if view.Path == "gameResult" {
		parsedTemplate, _ := template.ParseFiles("html/gameResult.html")
		err := parsedTemplate.Execute(view.Res, templateParams)

		if err != nil {
			fmt.Println("error rendering game result page")
			return
		}
	}
}