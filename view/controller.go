package view

import (
	"fmt"
	"net/http"
)

type View struct {
	Path string
	Res http.ResponseWriter
	Req *http.Request
}

var (
	homeView *HomeView =  &HomeView{}
	gameView *GameView = &GameView{}
	registerView *RegisterView = &RegisterView{}
	loginView *LoginView = &LoginView{}
	profileView *ProfileView = &ProfileView{}
)

func Serve(view *View, templateParams interface{}) {
	fmt.Println("hey")

	if view.Path == "home" {
		homeView.Serve(view, templateParams)
	} else if view.Path == "game" {
		gameView.Serve(view, templateParams)
	} else if view.Path == "games" {
		gameView.Serve(view, templateParams)
	} else if view.Path == "gameResult" {
		gameView.Serve(view, templateParams)
	} else if view.Path == "register" {
		registerView.Serve(view, templateParams)
	} else if view.Path == "login" {
		loginView.Serve(view, templateParams)
	} else if view.Path == "profile" {
		profileView.Serve(view, templateParams)
	}

}