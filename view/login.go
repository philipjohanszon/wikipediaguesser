package view

import (
	"fmt"
	"html/template"
)

type LoginView struct {
}


func (h *LoginView) Serve(view *View, templateParams interface{}) {
	

    parsedTemplate, _ := template.ParseFiles("html/login.html")
	err := parsedTemplate.Execute(view.Res, templateParams)

	if err != nil {
		fmt.Println("error rendering login page")
		return
	}
}