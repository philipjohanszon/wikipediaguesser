package view

import (
	"fmt"
	"html/template"
)

type RegisterView struct {
}


func (h *RegisterView) Serve(view *View, templateParams interface{}) {
	

    parsedTemplate, _ := template.ParseFiles("html/register.html")
	err := parsedTemplate.Execute(view.Res, templateParams)

	if err != nil {
		fmt.Println("error rendering register page")
		return
	}
	
}