package view

import (
	"fmt"
	"html/template"
)

type HomeView struct {
}

func (h *HomeView) Serve(view *View, templateParams interface{}) {

	fmt.Println(templateParams)

	parsedTemplate, _ := template.ParseFiles("html/home.html")
	err := parsedTemplate.Execute(view.Res, templateParams)

	if err != nil {
		fmt.Println("error rendering home page")
		return
	}

}
