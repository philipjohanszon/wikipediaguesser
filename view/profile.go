package view

import (
	"fmt"
	"html/template"
)

type ProfileView struct {
}


func (h *ProfileView) Serve(view *View, templateParams interface{}) {
	fmt.Println(templateParams)
	fmt.Println("im already here")

    parsedTemplate, _ := template.ParseFiles("html/profile.html")
	fmt.Println("it made it behind template parsing")
	err := parsedTemplate.Execute(view.Res, templateParams)
	fmt.Println("it made it behind templating")

	if err != nil {
		fmt.Println("error rendering profile page")
		fmt.Println(err.Error())
		return
	}
}