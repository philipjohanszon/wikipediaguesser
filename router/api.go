package router

import (
	"net/http"
	
)

type APIController struct {
}

var userController *UserController = &UserController{}

func (api *APIController) Handler(res http.ResponseWriter, req *http.Request) {
	var head string
	head, req.URL.Path = ShiftPath(req.URL.Path)

	if head == "login" {
		if req.Method == http.MethodPost {
			userController.Login(res, req)
		}
	} else if head == "register" {
		if req.Method == http.MethodPost {
			userController.Register(res, req)
		}
	} else if head == "startgame" {
		gameController.StartGame(res, req)
	} else if head == "verifygameanswer" {
		gameController.HandleGameAnswer(res, req)
	} else if head == "endgame" {
		gameController.EndGame(res, req)
	} 
}
