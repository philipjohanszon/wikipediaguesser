package router

import (
	"net/http"

	"wikiGuesser/view"
	"github.com/kataras/go-sessions/v3"
)

type RegisterController struct {
}

func (h *RegisterController) ServeHtml(res http.ResponseWriter, req *http.Request) {
	
	session := sessions.Start(res, req)

	if session.Get("userId") != nil {
		http.Redirect(res, req, "/games", http.StatusSeeOther)
	} else {
		templateParams := struct {
			UsernameAndEmailTaken bool
			UsernameTaken bool
			EmailTaken bool
			EmailDoesntMatch bool
			PasswordToWeak bool
		}{}

		usernameTaken := req.URL.Query().Get("usernameTaken")
		emailTaken := req.URL.Query().Get("emailTaken")
		usernameAndEmailTaken := req.URL.Query().Get("usernameAndEmailTaken")
		emailsDontMatch := req.URL.Query().Get("emailsDontMatch")
		passwordToWeak := req.URL.Query().Get("passwordToWeak")

		if usernameAndEmailTaken != "" {
			templateParams.UsernameAndEmailTaken = true
		}		

		if usernameTaken != "" {
			templateParams.UsernameTaken = true
		}

		if emailTaken != "" {
			templateParams.EmailTaken = true
		}

		if emailsDontMatch != "" {
			templateParams.EmailDoesntMatch = true
		}

		if passwordToWeak != "" {
			templateParams.PasswordToWeak = true
		}

		view.Serve(&view.View{
			Path: "register", 
			Res: res,
			Req: req,
		}, templateParams)
	}
}
