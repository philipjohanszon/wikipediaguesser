package router

import (
	"net/http"

	"wikiGuesser/view"
	"github.com/kataras/go-sessions/v3"
)

type LoginController struct {
}

func (h *LoginController) ServeHtml(res http.ResponseWriter, req *http.Request) {
	session := sessions.Start(res, req)

	if session.Get("userId") != nil {
		http.Redirect(res, req, "/games", http.StatusSeeOther)
	} else {
		templateParams := struct {
			LoginIncorrect bool
		}{}

		loginIncorrect := req.URL.Query().Get("loginIncorrect")

		if loginIncorrect != "" {
			templateParams.LoginIncorrect = true
		}		

		view.Serve(&view.View{
			Path: "login", 
			Res: res,
			Req: req,
		}, templateParams)
	}
}
