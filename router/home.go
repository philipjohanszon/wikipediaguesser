package router

import (
	"net/http"

	"wikiGuesser/model"
	"wikiGuesser/view"
	"github.com/kataras/go-sessions/v3"
)

type HomeController struct {
}

func (h *HomeController) Serve(res http.ResponseWriter, req *http.Request) {

	templateParams := struct {
		UserCount int
		QuestionCount int
		HighScore int
		LoggedIn bool
	} {
		UserCount: model.GetUserCount(),
		QuestionCount: model.GetQuestionCount(),
		HighScore: model.GetHighScore(),
	}

	session := sessions.Start(res, req)

	if session.Get("userId") != nil {
		templateParams.LoggedIn = true
	}

	view.Serve(&view.View{
		Path: "home",
		Res:  res,
		Req:  req,
	}, templateParams)
}
