package router

import (
	"fmt"
	"net/http"

	"wikiGuesser/model"

	"github.com/kataras/go-sessions/v3"
)

type UserController struct {
}

func (h UserController) Login(res http.ResponseWriter, req *http.Request) {
	if model.LoginUser(res, req) {
		http.Redirect(res, req, "/games", http.StatusSeeOther)
	} else {
		http.Redirect(res, req, "/login?loginIncorrect=true", http.StatusSeeOther)
	}
}

func (a *UserController) Register(res http.ResponseWriter, req *http.Request) {
	statusCode := model.RegisterUser(res, req)

	fmt.Println(statusCode)

	if statusCode == "registered" {
		http.Redirect(res, req, "/games", http.StatusSeeOther)
	} else if statusCode == "usernameAndEmailTaken" {
		http.Redirect(res, req, "/register?usernameAndEmailTaken=true", http.StatusSeeOther)
	} else if statusCode == "emailTaken" {
		http.Redirect(res, req, "/register?emailTaken=true", http.StatusSeeOther)
	} else if statusCode == "usernameTaken" {
		http.Redirect(res, req, "/register?usernameTaken=true", http.StatusSeeOther)
	} else if statusCode == "bad request" {
		http.Error(res, "bad request, play with the api", 400)
	} else if statusCode == "emailsDontMatch" {
		http.Redirect(res, req, "/register?emailsDontMatch=true", http.StatusSeeOther)
	} else if statusCode == "passwordToWeak" {
		http.Redirect(res, req, "/register?passwordToWeak=true", http.StatusSeeOther)
	}
}

func (a *UserController) Logout(res http.ResponseWriter, req *http.Request) {
	session := sessions.Start(res, req)
	session.Destroy()

	http.Redirect(res, req, "/login", http.StatusSeeOther)
}
