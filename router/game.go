package router

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"wikiGuesser/model"
	"wikiGuesser/view"

	"github.com/kataras/go-sessions/v3"
)

type GameController struct {
}

func (h *GameController) ServeHtml(head string, res http.ResponseWriter, req *http.Request) {
	session := sessions.Start(res, req)

	fmt.Println(head)

	if head == "game" {
		if fmt.Sprintf("%v", session.Get("gameId")) != "" {

			if fmt.Sprintf("%v", session.Get("refreshLock")) != "on" {

				gameData := model.GetGameData(session.Get("gameId"))

				if gameData == nil {
					http.Redirect(res, req, "/games", http.StatusSeeOther)
				} else {

					gameDataToBeDisplayed := struct {
						Titles     [5]string
						ImgSrc     string
						TimeLeft   interface{}
						ScoreCount interface{}
					}{
						Titles:     gameData.Titles,
						ImgSrc:     gameData.ImgSrc,
						TimeLeft:   session.Get("gameTimeLeft"),
						ScoreCount: gameData.ScoreCount,
					}

					session.Set("CorrectTitle", gameData.CorrectTitle)
					session.Set("questionId", gameData.QuestionId)
					session.Set("gameDisplayedTime", time.Now().Format(time.UnixDate))

					if session.Get("userId") != nil {
						session.Set("refreshLock", "on")
						view.Serve(&view.View{
							Path: "game",
							Res:  res,
							Req:  req,
						}, gameDataToBeDisplayed)
					} else {
						http.Redirect(res, req, "/login", http.StatusSeeOther)
					}
				}
			} else {
				gameId := model.HandleMidGameRefresh(res, req)
				redirectUrl := "/gameResult?gameId=" + fmt.Sprintf("%v", gameId)

				http.Redirect(res, req, redirectUrl, http.StatusSeeOther)
			}

		} else {
			http.Redirect(res, req, "/games", http.StatusSeeOther)
		}
	} else if head == "games" {
		view.Serve(&view.View{
			Path: "games",
			Res:  res,
			Req:  req,
		}, nil)
	} else if head == "gameResult" {
		fmt.Println("hello worlddd")
		gameId := req.URL.Query().Get("gameId")
		gameIdInt, err := strconv.Atoi(gameId)

		if err != nil {
			http.ServeFile(res, req, "html/404.html")
		} else {
			if gameId != "" {

				templateParams := model.GetGameResults(gameIdInt)

				if templateParams != nil {
					fmt.Println("serving gameresult")

					view.Serve(&view.View{
						Path: "gameResult",
						Res:  res,
						Req:  req,
					}, templateParams)
				} else {
					http.Error(res, "Couldnt get game data", 500)
				}

			} else {
				http.Redirect(res, req, "/games", http.StatusSeeOther)
			}
		}
	}
}

func (h *GameController) StartGame(res http.ResponseWriter, req *http.Request) {
	var head string
	head, req.URL.Path = ShiftPath(req.URL.Path)

	session := sessions.Start(res, req)
	userId := session.Get("userId")

	if userId != nil {
		if head == "original" {
			status := model.StartGame(1, res, req)

			if status != false {
				http.Redirect(res, req, "/game", http.StatusSeeOther)
			} else {
				http.Error(res, "There was a problem starting the game", 500)
			}
		} else if head == "ultrafast" {
			status := model.StartGame(2, res, req)

			if status != false {
				http.Redirect(res, req, "/game", http.StatusSeeOther)
			} else {
				http.Error(res, "There was a problem starting the game", 500)
			}
		}
	} else {
		http.Redirect(res, req, "/login", http.StatusSeeOther)
	}
}

func (h *GameController) HandleGameAnswer(res http.ResponseWriter, req *http.Request) {
	session := sessions.Start(res, req)
	userId := session.Get("userId")

	if userId != nil {
		continueGame := model.HandleGameAnswer(res, req)

		if continueGame {
			http.Redirect(res, req, "/game", http.StatusSeeOther)
		} else {
			gameId := session.Get("gameId")
			session.Set("gameId", "")
			redirectUrl := "/gameResult?gameId=" + fmt.Sprintf("%v", gameId)

			http.Redirect(res, req, redirectUrl, http.StatusSeeOther)
		}

	} else {
		http.Error(res, "you need to be logged in", 400)
	}
}

func (h *GameController) EndGame(res http.ResponseWriter, req *http.Request) {
	session := sessions.Start(res, req)

	gameId := session.Get("gameId")
	session.Set("gameId", "")

	if gameId != "" && session.Get("userId") != nil {
		model.EndCurrentGame(res, req)
	}

	http.Redirect(res, req, "/gameResult?gameId="+fmt.Sprintf("%v", gameId), http.StatusSeeOther)

}
