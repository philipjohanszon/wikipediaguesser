package router

import (
	"fmt"
	"net/http"
	"path"
	"strings"
)

type App struct {
}

var (
	loginController    *LoginController = &LoginController{}
	registerController *RegisterController = &RegisterController{}
	apiController      *APIController = &APIController{}
	homeController *HomeController = &HomeController{}
	gameController *GameController = &GameController{}
	profileController *ProfileController = &ProfileController{}
)

func (app *App) Serve(res http.ResponseWriter, req *http.Request) {

	fmt.Println(req.URL.Path)
	
	var head string
	head, req.URL.Path = ShiftPath(req.URL.Path)


	if head == "" {
		homeController.Serve(res, req)
	} else if head == "login" {
		loginController.ServeHtml(res, req)
	} else if head == "register" {
		registerController.ServeHtml(res, req)
	} else if head == "game" {
		gameController.ServeHtml(head, res, req)
	} else if head == "games" {
		gameController.ServeHtml(head ,res, req)
	} else if head == "gameResult" {
		gameController.ServeHtml(head, res, req)
	} else if head == "profile" {
		profileController.ServeHtml(res, req)
	} else if head == "logout" {
		userController.Logout(res, req)
	} else if head == "api" {
		apiController.Handler(res, req)
	} else {
		http.ServeFile(res, req, "html/404.html")
	}
}

func ShiftPath(pathString string) (head, tail string) {
	pathString = path.Clean("/" + pathString)
	i := strings.Index(pathString[1:], "/") + 1
	if i <= 0 {
		return pathString[1:], "/"
	}
	return pathString[1:i], pathString[i:]
}