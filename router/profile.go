package router

import (
	"fmt"
	"net/http"
	"strconv"

	"wikiGuesser/model"
	"wikiGuesser/view"
	"github.com/kataras/go-sessions/v3"
)

type ProfileController struct {
}

func (h *ProfileController) ServeHtml(res http.ResponseWriter, req *http.Request) {
	session := sessions.Start(res, req)
	var templateParams interface{}

	inputUserId := req.URL.Query().Get("id")

	if inputUserId != "" {

		userId, errInt := strconv.ParseInt(fmt.Sprintf("%v", inputUserId), 10, 64)

		if errInt == nil {

			templateParams = model.GetProfileData(int(userId))

		} else {
			http.Redirect(res, req, "/notfound", http.StatusSeeOther)
		}

	} else {
		if session.Get("userId") != nil {
			userId, err := strconv.Atoi(fmt.Sprintf("%v", session.Get("userId")))

			if err != nil {
				http.Redirect(res, req, "/notfound", http.StatusSeeOther)
				return
			}

			templateParams = model.GetProfileData(userId)
		} else {
			http.Redirect(res, req, "/notfound", http.StatusSeeOther)
			return
		}
	}

	if templateParams != nil {
		view.Serve(&view.View{
			Res: res,
			Req: req,
			Path: "profile",
		}, templateParams)
	} else {
		http.Redirect(res, req, "/notfound", http.StatusSeeOther)
	}

}
