//https://en.wikipedia.org/w/api.php?action=query&generator=random&prop=pageimages&format=json&pithumbsize=100
//https://en.wikipedia.org/w/api.php?format=json&action=query&generator=random&grnnamespace=0&prop=pageimages&format=json&pithumbsize=400&iiprop=extmetadata <- thumbnail height
//https://commons.wikimedia.org/w/api.php?action=query&format=json&formatversion=2&prop=imageinfo&iiprop=url&iiurlwidth=320&titles=File:Laxmikant_Shetgaonkar_at_IFFI_Press_Conference_in_2011_(cropped).jpgvar mysql      = require('mysql');

//To get image and title:
//prop=pageprops

//Denna ger thumbnail (file och url) och page id och title 
//https://en.wikipedia.org/w/api.php?action=query&generator=random&prop=pageimages&format=json&pithumbsize=1000&pilicense=free

//Med filename så kan man hitta copyright status
//http://en.wikipedia.org/w/api.php?action=query&prop=imageinfo&iiprop=extmetadata&titles=File%3aUSA_North_Carolina_location_map.svg&format=json


/*
{"batchcomplete":"","continue":{"grncontinue":"0.734836146966|0.7348364010470001|27613079|0","continue":"grncontinue||"},
"query":{"pages":{"52434502":{"pageid":52434502,"ns":0,"title":"Penn Tower","pageprops":{"kartographer_frames":"1","page_image_free":"First_National_Bank_Building_Penn_Tower_Williamsport.jpg","wikibase_item":"Q28104171"}}}}}
*/

//http://en.wikipedia.org/w/api.php?action=query&titles=Al-Farabi&prop=pageimages&format=json&pithumbsize=100


//To get category:
//prop=categories

const mysql = require('mysql');
let fetch = require("node-fetch");

const _downloadAmount = 5;
const categoryId = 1;

var connection = mysql.createConnection({
  host: "127.0.0.1",
  user: "root",
  password: "",
});

connection.connect(function (err) {
  if (err) {
    console.error("error connecting: " + err.stack);
    return;
  }

  console.log("connected as id " + connection.threadId);

  downloadQuestions();
});

async function downloadQuestions() {
  for (let i = 0; i < _downloadAmount; i++) {
    finishedObj = await getData();
    if (finishedObj != null) {
      console.log("finishedobj:\n"+finishedObj.query)
      //insertIntoDatabase(finishedObj);
    }
    else {
      //makes so that it doesnt go up
      i--;
    }
  }
}

async function getData() {
  let imageInfo = await getImageInfo();
  
  if (imageInfo == null) {
    return null
  }
  else {
    let firstKey = getFirstKey(imageInfo.query.pages);

    let licenseData = await getLicenseData(imageInfo.query.pages.firstKey.pageimage);

    if (licenseAllowed(licenseData)) {
      return imageInfo
    }
    else {
      return null
    }
  }
}



function getImageInfo() {
  fetch("https://en.wikipedia.org/w/api.php?action=query&generator=random&prop=pageimages&format=json&pithumbsize=1000&pilicense=free")
    .then(res => res.json())
    .then(json => handleImageInfo(json))
}

function handleImageInfo(json) {
  if (verifyImageInfo(json)) {
    return json
  }
  else {
    return null
  }

}

function verifyImageInfo(response) {
  try {
    let firstKey = getFirstKey(response.query.pages);

    //if it has thumbnail and pageimage
    if (hasKey(response.query.pages.firstKey, "thumbnail") && hasKey(response.query.pages.firstKey), "pageimage") {
      if (titlePermitted(response.query.pages.firstKey.title)) {
        if (imageCorrectSize(imgObj)) {
          return true;
        }
      }
    }
    
    return false
    
    
  } catch (error) {
    return false;
  }
}

function titlePermitted(title) {
  if (title.length < 32 || title.length > 4) {
    //if : doesnt appear in title, some titles have File:filename.svg
    if (!(title.split(":").length-1 > 0)) {
      return true
    }
  }
  return false
}

function imageCorrectSize(imgObj) {
  if (imgObj.width > 500 && imgObj.height > 500) {
    return true
  } 
  else {
    return false
  }
}

function getLicenseData(filename) {
  fetch("http://en.wikipedia.org/w/api.php?action=query&prop=imageinfo&iiprop=extmetadata&format=json&titles=File%3a" + filename)
    .then(res => res.json())
    .then(function (json) {
      return json
    })
}

function licenseAllowed (licenseData) {
  if (licenseData.query.pages["-1"].imageinfo.extmetadata.AttributionRequired.value === "false" && 
      licenseData.query.pages["-1"].imageinfo.extmetadata.Copyrighted.value === "False") {
        return true
  }
  else {
    return false
  }
}

//GENERAL

function hasKey(obj, key) {
  if (key in obj) {
    return true
  }
  else {
    return false
  }
}

function getFirstKey(obj) {
  for (key in obj) {
      return key;
  }
}