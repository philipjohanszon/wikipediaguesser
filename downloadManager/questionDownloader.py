import requests
import mysql.connector
import json

_amount = 5
_categoryId = 1
_db = mysql.connector.connect(
    host="localhost", 
    user="wikiguesser",
    password="Johanszon12345!",
    database="wikipediaguesser"
)
_cursor = _db.cursor()

#print(_db)

def downloadQuestions():
    i = 0
    while i < _amount:
        print("\n\n")
        try:
            imageData = getData()

            if imageData != None:
                print("this data was good\n")
                print(imageData)

                insertIntoDb(imageData)

            else:
                print("data was not good")
                i = i-1
        except:
            i = i-1
            print("it killed itself again")


def getData():
    imageData = getImageData()

    print(imageData)
    
    if imageData != None:
            licenseData = getLicenseData(imageData["pageimage"])

            firstKey = list(licenseData["query"]["pages"].keys())[0]

            licenseData = licenseData["query"]["pages"][firstKey]["imageinfo"][0]["extmetadata"]

            if licenseIsAllowed(licenseData):
                return imageData
            else:
                return None
    else:
        return None

def getImageData():
    request = requests.get("https://en.wikipedia.org/w/api.php?action=query&generator=random&prop=pageimages&format=json&pithumbsize=1000&pilicense=free")
    imageData = json.loads(request.text)

    #removes all of the useless json infront o the page json
    firstKey = list(imageData["query"]["pages"].keys())[0]
    imageData = imageData["query"]["pages"][firstKey]

    if verifyImageInfo(imageData):
        return imageData
    else:
        return None

def verifyImageInfo(imageData):
    print(imageData)
    if "thumbnail" in imageData and "pageimage" in imageData:
        print("it has a image!")
        if titlePermitted(imageData["title"]):
            print("title was allowed!")
            if imageCorrectSize(imageData["thumbnail"]):
                print("size was correct!")
                return True

    return False

def titlePermitted(title):
    if len(title) <= 32 and len(title) >= 3:
        print("title length is good")
        if not("File:" in title):
            return True

    return False

def imageCorrectSize(imageData):
    if imageData["width"] > 500 and imageData["height"] > 500:
        return True
    else:
        return False

def getLicenseData(filename):
    url = "http://en.wikipedia.org/w/api.php?action=query&prop=imageinfo&iiprop=extmetadata&format=json&titles=File%3a" + filename

    request = requests.get(url)

    return json.loads(request.text)

def licenseIsAllowed(licenseData):
    if "AttributionRequired" in licenseData and "Copyrighted" in licenseData:
        if licenseData["AttributionRequired"]["value"] == "false" and licenseData["Copyrighted"]["value"] == "False":
            return True

    return False

def insertIntoDb(imageData):
    titleSql = "INSERT INTO titles (name) VALUES(%s)"
    titleValue = (imageData["title"],)

    _cursor.execute(titleSql, titleValue) 

    questionSql = "INSERT INTO questions (titleId, img, width, height) VALUES (%s, %s, %s, %s)"
    questionValues = (_cursor.lastrowid, imageData["thumbnail"]["source"], imageData["thumbnail"]["width"], imageData["thumbnail"]["height"])
    
    _cursor.execute(questionSql, questionValues)

    categorySql = "INSERT INTO questioncategory (questionId, categoryId) VALUES (%s, %s)"
    categoryValues = (_cursor.lastrowid, _categoryId)

    _cursor.execute(categorySql, categoryValues)

    _db.commit()

downloadQuestions()
